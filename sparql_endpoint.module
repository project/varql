<?php

/**
 * Implements hook_menu().
 */
function sparql_endpoint_menu() {
  $items['sparql_endpoint/%sparql_endpoint'] = array(
    'title' => 'SPARQL Endpoint',
    'title callback' => 'sparql_endpoint_page_title',
    'title arguments' => array(1),
    'page callback' => 'sparql_endpoint_page',
    'page arguments' => array(1),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items['sparql_endpoint/%sparql_endpoint/view'] = array(
    'title' => 'View',
    'access callback' => TRUE,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['sparql_endpoint/%sparql_endpoint/edit'] = array(
    'title' => 'Edit',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sparql_endpoint_form_edit', 1),
    'access callback' => TRUE, 
    'type' => MENU_LOCAL_TASK,
    'weight' => 10,
  );

  $items['sparql_endpoint/%sparql_endpoint/delete'] = array(
    'title' => 'Delete',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('sparql_endpoint_delete_confirm', 1),
    'access callback' => TRUE,
    'weight' => 10,
    'type' => MENU_LOCAL_TASK,
  );
  $items['admin/structure/sparql_endpoint'] = array(
    'title' => 'SPARQL Endpoints Registry',
    'description' => 'Manage sparql endpoint entities.',
    'access callback' => TRUE,
    'page callback' => 'sparql_endpoint_page_admin',
    'page arguments' => array('list'),
    'weight' => -10,
  );

  $items['admin/structure/sparql_endpoint/list'] = array(
    'title' => 'List',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -10,
  );

  $items['admin/structure/sparql_endpoint/create'] = array(
    'title' => 'Add SPARQL endpoint',
    'page arguments' => array('create'),
    'access callback' => TRUE,
    'type' => MENU_LOCAL_ACTION,
  );

  return $items;
}

/**
 * Implements hook_entity_info().
 */
function sparql_endpoint_entity_info() {
  $return = array(
    'sparql_endpoint' => array(
      'label' => t('SPARQL Endpoint'),
      'base table' => 'sparql_endpoint',
      'uri callback' => 'sparql_endpoint_uri',
      'fieldable' => TRUE,
      'entity keys' => array(
        'id' => 'seid',
      ),
      'bundles' => array(
        'sparql_endpoint' => array(
          'label' => t('SPARQL Endpoint'),
          'admin' => array(
            'path' => 'admin/structure/sparql_endpoint',
          ),
        ),
      ),
    ),
  );
  return $return;
}

/**
 * Entity uri callback.
 */
function sparql_endpoint_uri($sparql_endpoint) {
  return array(
    'path' => 'sparql_endpoint/' . $sparql_endpoint->seid,
  );
}

/**
 * Implements hook_admin_paths().
 */
function sparql_endpoint_admin_paths() {
  $paths = array(
    'sparql_endpoint/*/edit' => TRUE,
    'sparql_endpoint/*/delete' => TRUE,
  );
  return $paths;
}

function sparql_endpoint_load($seid, $reset = FALSE) {
  $sparql_endpoint = sparql_endpoint_load_multiple(array($seid), array(), $reset);
  return reset($sparql_endpoint);
}

function sparql_endpoint_load_multiple($seids = array(), $conditions = array(), $reset = FALSE) {
  return entity_load('sparql_endpoint', $seids, $conditions, $reset);
}

function sparql_endpoint_delete($seid) {
  sparql_endpoint_delete_multiple(array($seid));
}

function sparql_endpoint_delete_multiple($seids) {
  if (!empty($seids)) {
    db_delete('sparql_endpoint')
     ->condition('seid', $seids, 'IN')
     ->execute();

    entity_get_controller('sparql_endpoint')->resetCache();
  }
}

function sparql_endpoint_page_title($sparql_endpoint) {
  return check_plain($sparql_endpoint->title);
}

function sparql_endpoint_page($sparql_endpoint) {
  // The module provides only one view mode.
  $view_mode = 'default';

  // Remove previously built content, if exists.
  $sparql_endpoint->content = array(
    'endpoint' => $sparql_endpoint->endpoint,
    'dataset'  => $sparql_endpoint->dataset,
  );
  

  drupal_set_title($sparql_endpoint->title);
  // Build fields content.
  field_attach_prepare_view('sparql_endpoint', array($sparql_endpoint->seid => $sparql_endpoint), $view_mode);
  entity_prepare_view('sparql_endpoint', array($sparql_endpoint->seid => $sparql_endpoint));

  $build = field_attach_view('sparql_endpoint', $sparql_endpoint, $view_mode);
  return $build;
}

/**
 * Implements hook_field_extra_fields().
 */
function sparql_endpoint_field_extra_fields() {
  $return = array();
  $return['sparql_endpoint']['sparql_endpoint'] = array(
    'form' => array(
      'title' => array(
        'label' => t('Title'),
        'description' => t('Title'),
        'weight' => -10,
      ),
      'endpoint' => array(
        'label' => t('Endpoint'),
        'description' => t('Endpoint URL.'),
        'weight' => -8,
      ),
      'dataset' => array(
        'label' => t('Dataset'),
        'description' => t('Dataset'),
        'weight' => -6,
      ),
    ),
  );

  return $return;
}

function sparql_endpoint_save(&$edit) {
  field_attach_presave('sparql_endpoint', $edit);
  if (!empty($edit->seid)) {
  	drupal_write_record('sparql_endpoint', $edit, 'seid');
    field_attach_update('sparql_endpoint', $edit);
    module_invoke_all('entity_update', 'sparql_endpoint', $edit);
  	return $edit;
  }
  drupal_write_record('sparql_endpoint', $edit);
	field_attach_insert('sparql_endpoint', $edit);
	module_invoke_all('entity_insert', 'sparql_endpoint', $edit);
  return $edit;
}

function sparql_endpoint_page_admin($tab = '') {
  switch ($tab) {
    case 'create':
      $build['sparql_endpoint_create'] = drupal_get_form('sparql_endpoint_form_edit');
      break;
    default:
      $build['sparql_endpoint_list'] = drupal_get_form('sparql_endpoint_form_list');
  }
  return $build;
}

function sparql_endpoint_form_list() {
  $header = array(
    'title' => array('data' => t('Title'), 'field' => 'ce.title'),
    'endpoint' => array('data' => t('Endpoint'), 'field' => 'endpoint'),
    'dataset' => array('data' => t('Dataset'), 'field' => 'dataset'),
    'edit' => array('data' => t('Edit')),
    'delete' => array('data' => t('Delete')),
  );
  $query = db_select('sparql_endpoint', 'ce');
  $count_query = clone $query;
  $count_query->addExpression('COUNT(ce.seid)');

  $query = $query->extend('PagerDefault')->extend('TableSort');
  $query
    ->fields('ce', array('seid', 'title', 'endpoint', 'dataset'))
    ->limit(20)
    ->orderByHeader($header)
    ->setCountQuery($count_query);
  $result = $query->execute();

  $destination = drupal_get_destination();

  $options = array();
  foreach ($result as $row) {
    $options[$row->seid] = array(
      'title' => $row->title,
      'endpoint' => $row->endpoint,
      'dataset' => $row->dataset,
      'edit' => array('data' => array(
        '#type' => 'link',
        '#title' => t('edit'),
        '#href' => "sparql_endpoint/$row->seid/edit",
        '#options' => array('query' => $destination),
      )),
      'delete' => array('data' => array(
        '#type' => 'link',
        '#title' => t('delete'),
        '#href' => "sparql_endpoint/$row->seid/delete",
        '#options' => array('query' => $destination),
      )),
    );
  }

  $form['sparql_endpoint'] = array(
    '#type' => 'tableselect',
    '#header' => $header,
    '#options' => $options,
    '#empty' => t('No entities available.'),
  );
  $form['pager']['#markup'] = theme('pager');

  return $form;
}

function sparql_endpoint_form_edit($form, &$form_state, $edit = NULL) {
  if (!isset($edit)) {
    $edit = (object) array(
      'title' => '',
      'endpoint' => '',
      'dataset' => '',
    );
  }
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => $edit->title,
    '#required' => TRUE,
  );
  $form['endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Endpoint'),
    '#default_value' => $edit->endpoint,
    '#required' => TRUE,
  );
  $form['dataset'] = array(
    '#type' => 'textfield',
    '#title' => t('Dataset'),
    '#default_value' => $edit->dataset,
    '#required' => FALSE,
  );

  // Attach fields from Field module.
  field_attach_form('sparql_endpoint', (object) $edit, $form, $form_state);

  // Store ID if any.
  if (!empty($edit->seid)) {
    $form['seid'] = array(
      '#type' => 'value',
      '#value' => $edit->seid,
    );
  }
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 5,
  );

  return $form;
}

function sparql_endpoint_form_edit_validate($form, &$form_state) {
  // Attach validation from Field module.
  field_attach_form_validate('sparql_endpoint', (object) $form_state['values'], $form, $form_state);
}

function sparql_endpoint_form_edit_submit($form, &$form_state) {
  $edit = (object) $form_state['values'];
  // Attach submit handlers from Field module.
  field_attach_submit('sparql_endpoint', $edit, $form, $form_state);
  // Save own data.
  sparql_endpoint_save($edit);
  $form_state['redirect'] = "sparql_endpoint/$edit->seid";
}

function sparql_endpoint_delete_confirm($form, &$form_state, $sparql_endpoint) {
  $form['#sparql_endpoint'] = $sparql_endpoint;
  $form['seid'] = array('#type' => 'value', '#value' => $sparql_endpoint->seid);
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $sparql_endpoint->title)),
    'sparql_endpoint/' . $sparql_endpoint->seid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

function sparql_endpoint_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $sparql_endpoint = sparql_endpoint_load($form_state['values']['seid']);
    sparql_endpoint_delete($form_state['values']['seid']);
    watchdog('sparql_endpoint', 'deleted %title.', array('%title' => $sparql_endpoint->title));
    drupal_set_message(t('%title has been deleted.', array('%title' => $sparql_endpoint->title)));
  }

  $form_state['redirect'] = '<front>';
}
